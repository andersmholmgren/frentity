// Copyright (c) 2014, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library frentity.example;

import 'package:observe/observe.dart';

main() {
//  var awesome = new Awesome();
//  print('awesome: ${awesome.isAwesome}');
  final list1 = [1, 2, 3, 4];

  final list2 = [2, 3, 4];

  final changes = ObservableList.calculateChangeRecords(list1, list2);
  print('-------- changes: # ${changes.length}: $changes');
}
