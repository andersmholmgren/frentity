// Copyright (c) 2014, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library frentity.test;

import 'package:test/test.dart';

import 'actions/all_tests.dart' as actions;
import 'utils/all_tests.dart' as utils;

main() {
  group('[actions]', actions.main);
  group('[utils]', utils.main);
}
