library utils.test;

import 'package:test/test.dart';
import 'frappe_utils_test.dart' as frappe_utils;
import 'observe_utils_test.dart' as observe_utils;
import 'pager_test.dart' as pager;

main() {
  group('[frappe_utils]', frappe_utils.main);
  group('[observe_utils]', observe_utils.main);
  group('[pager]', pager.main);
}
