library pager.test;

import 'package:test/test.dart';
import 'package:frappe/frappe.dart';
import 'package:option/option.dart';
import 'dart:async';
import 'package:frentity/src/utils/pager.dart';
import 'package:hateoas_models/hateoas_models.dart';
import 'package:mockito/mockito.dart';
import 'package:frentity/src/actions/action.dart';

//typedef Future<SearchResult<T>> Fetcher<T>(int page);

abstract class FetcherClass<T> {
  Future<SearchResult<T>> call(int page);
}

class MockFetcher<T> extends Mock implements FetcherClass<T> {
  noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}

main() {
  const List emptyList = const [];
  const List singleList1 = const [1];
  const List singleList2 = const [2];

  SearchResult _sr(List list,
          [int totalCount = 1, int numPages = 1, int page = 1]) =>
      new SearchResult(totalCount, numPages, page, list);

  group('firstPage', () {
    MockFetcher fetcher;
    Paged paged;

    setUp(() {
      fetcher = new MockFetcher();
      paged = new Paged((int page) => fetcher.call(page));
    });

    group('when search result is empty', () {
      setUp(() {
        when(fetcher.call(any)).thenReturn(new Future.value(_sr(emptyList, 0)));
      });

      test('completes', () {
        expect(paged.firstPage, completes);
      });

      group('completes with', () {
        Page firstPage;
        SearchResult result;

        setUp(() async {
          firstPage = await paged.firstPage;
          result = firstPage.result;
        });

        test('calls fetcher with page 1', () {
          verify(fetcher.call(1)).called(1);
          verifyNoMoreInteractions(fetcher);
        });

        test('a single page', () {
          expect(result.numPages, 1);
        });

        test('page 1', () {
          expect(result.page, 1);
        });

        test('total count 0', () {
          expect(result.totalCount, 0);
        });

        test('page size 0', () {
          expect(result.pageSize, 0);
        });

        test('empty results', () {
          expect(result.results, isEmpty);
        });

        test('no previous page', () {
          expect(firstPage.previousPage, new isInstanceOf<None>());
        });

        test('no next page', () {
          expect(firstPage.nextPage, new isInstanceOf<None>());
        });
      });
    });

    group('when two pages of single item search result', () {
      setUp(() {
        when(fetcher.call(1))
            .thenReturn(new Future.value(_sr(singleList1, 2, 2, 1)));
        when(fetcher.call(2))
            .thenReturn(new Future.value(_sr(singleList1, 2, 2, 2)));
      });

      test('completes', () {
        expect(paged.firstPage, completes);
      });

      group('completes with', () {
        Page firstPage;
        SearchResult result;

        setUp(() async {
          firstPage = await paged.firstPage;
          result = firstPage.result;
        });

        test('calls fetcher with page 1', () {
          verify(fetcher.call(1)).called(1);
          verifyNoMoreInteractions(fetcher);
        });

        test('two pages', () {
          expect(result.numPages, 2);
        });

        test('page 1', () {
          expect(result.page, 1);
        });

        test('total count 2', () {
          expect(result.totalCount, 2);
        });

        test('page size 1', () {
          expect(result.pageSize, 1);
        });

        test('a single item in results', () {
          expect(result.results, hasLength(1));
        });

        test('no previous page', () {
          expect(firstPage.previousPage, new isInstanceOf<None>());
        });

        test('a next page', () {
          expect(firstPage.nextPage, new isInstanceOf<Some>());
        });

        test('a next page that completes', () {
          expect(firstPage.nextPage.get(), completes);
        });

        group('a next page that completes with', () {
          Page nextPage;
          SearchResult result;

          setUp(() async {
            clearInteractions(fetcher);
            nextPage = await firstPage.nextPage.get();
            result = nextPage.result;
          });

          test('calls fetcher with page 2', () {
            verify(fetcher.call(2)).called(1);
            verifyNoMoreInteractions(fetcher);
          });

          test('two pages', () {
            expect(result.numPages, 2);
          });

          test('page 2', () {
            expect(result.page, 2);
          });

          test('total count 2', () {
            expect(result.totalCount, 2);
          });

          test('page size 1', () {
            expect(result.pageSize, 1);
          });

          test('a single item in results', () {
            expect(result.results, hasLength(1));
          });

          test('no next page', () {
            expect(nextPage.nextPage, new isInstanceOf<None>());
          });

          test('a previous page', () {
            expect(nextPage.previousPage, new isInstanceOf<Some>());
          });

          test('a previous page that completes', () {
            expect(nextPage.previousPage.get(), completes);
          });

          group('a previous page that completes with', () {
            Page previousPage;
            SearchResult result;

            setUp(() async {
              clearInteractions(fetcher);
              previousPage = await nextPage.previousPage.get();
              result = previousPage.result;
            });

            test('calls fetcher with page 1', () {
              verify(fetcher.call(1)).called(1);
              verifyNoMoreInteractions(fetcher);
            });

            test('two pages', () {
              expect(result.numPages, 2);
            });

            test('page 1', () {
              expect(result.page, 1);
            });

            test('total count 2', () {
              expect(result.totalCount, 2);
            });

            test('page size 1', () {
              expect(result.pageSize, 1);
            });

            test('a single item in results', () {
              expect(result.results, hasLength(1));
            });
          });
        });
      });
    });
  });

  group('all', () {
    MockFetcher fetcher;
    Paged paged;

    setUp(() {
      fetcher = new MockFetcher();
      paged = new Paged((int page) => fetcher.call(page));
    });

    group('when search result is empty', () {
      setUp(() {
        when(fetcher.call(any)).thenReturn(new Future.value(_sr(emptyList, 0)));
      });

      group('resultStream', () {
        Stream<SearchResult> resultStream;
        Future<List<SearchResult>> resultsFuture;

        setUp(() {
          resultStream = paged.all.resultStream;
          resultsFuture = resultStream.toList();
        });

        test('completes', () {
          expect(resultsFuture, completes);
        });

        group('completes with', () {
          List<SearchResult> results;

          setUp(() async {
            results = await resultsFuture;
          });

          test('calls fetcher with page 1', () {
            verify(fetcher.call(1)).called(1);
            verifyNoMoreInteractions(fetcher);
          });

          test('a single result', () {
            expect(results, hasLength(1));
          });

          test('a single result that has no items', () {
            expect(results.first.results, isEmpty);
          });
        });
      });

      group('percentComplete', () {
        Property<PercentComplete> percentComplete;
        Future<List<PercentComplete>> futureValues;

        setUp(() {
          percentComplete = paged.all.percentComplete;
          futureValues = percentComplete.take(2).toList();
        });

        test('completes', () {
          expect(futureValues, completes);
        });

        test('starts at 0/10 and ends in 0/0', () {
          expect(futureValues, completion(orderedEquals([
            new PercentComplete().setTotal(10.0),
            new PercentComplete().complete()
          ])));
        });

        // how can we avoid tests like this??
//        skip_test('contain only the expected values', () async {
//          await new Future.delayed(const Duration(milliseconds: 1));
//          expect(percentCompleteValues.received, orderedEquals([
//            new PercentComplete().setTotal(10.0),
//            new PercentComplete().complete()
//          ]));
//        });
      });
    });

    group('when two pages of single item search result', () {
      setUp(() {
        when(fetcher.call(1))
            .thenReturn(new Future.value(_sr(singleList1, 2, 2, 1)));
        when(fetcher.call(2))
            .thenReturn(new Future.value(_sr(singleList2, 2, 2, 2)));
      });

      group('resultStream', () {
        Stream<SearchResult> resultStream;
        Future<List<SearchResult>> resultsFuture;

        setUp(() {
          resultStream = paged.all.resultStream;
          resultsFuture = resultStream.toList();
        });

        test('completes', () {
          expect(resultsFuture, completes);
        });

        group('completes with', () {
          List<SearchResult> results;

          setUp(() async {
            results = await resultsFuture;
          });

          test('calls fetcher with page 1 and 2', () {
            verify(fetcher.call(1)).called(1);
            verify(fetcher.call(2)).called(1);
            verifyNoMoreInteractions(fetcher);
          });

          test('two results', () {
            expect(results, hasLength(2));
          });

          test('a first result that has a single item', () {
            expect(results.first.results, equals([1]));
          });

          test('a second result that has a single item', () {
            expect(results.elementAt(1).results, equals([2]));
          });
        });
      });

      group('percentComplete', () {
        Property<PercentComplete> percentComplete;
        Future<List<PercentComplete>> futureValues;

        setUp(() {
          percentComplete = paged.all.percentComplete;
          futureValues = percentComplete.take(3).toList();
        });

        test('completes', () {
          expect(futureValues, completes);
        });

        test('starts at 0/10 and ends in 2/2', () {
          expect(futureValues, completion(orderedEquals([
            new PercentComplete().setTotal(10.0),
            new PercentComplete().setTotal(2.0).addToCurrent(1.0),
            new PercentComplete().setTotal(2.0).complete()
          ])));
        });
      });
    });

    group('when two pages with first throwing exeption', () {
      setUp(() {
        when(fetcher.call(1)).thenAnswer((_) => new Future.error("doh"));

        when(fetcher.call(2))
            .thenReturn(new Future.value(_sr(singleList2, 2, 2, 2)));
      });

      group('resultStream', () {
        Future<List<SearchResult>> resultsFuture;

        setUp(() {
          resultsFuture = paged.all.resultStream.toList();
        });

        test('completes with exception', () {
          expect(resultsFuture, throws);
        });

        group('completes with', () {
          var exception;

          setUp(() async {
            try {
              await resultsFuture;
            } catch (e) {
              exception = e;
            }
          });

          test('calls fetcher with page 1', () {
            verify(fetcher.call(1)).called(1);
            verifyNoMoreInteractions(fetcher);
          });

          test('expected error', () {
            expect(exception, equals("doh"));
          });
        });
      });

      group('percentComplete', () {
        Future<List<PercentComplete>> futureValues;

        setUp(() {
          futureValues = paged.all.percentComplete.take(1).toList();
        });

        test('completes', () {
          expect(futureValues, completes);
        });

        test('starts at 0/10 and remains there', () {
          expect(futureValues, completion(
              orderedEquals([new PercentComplete().setTotal(10.0)])));
        });
      });
    });

    group('when two pages with second throwing exeption', () {
      setUp(() {
        when(fetcher.call(1))
            .thenReturn(new Future.value(_sr(singleList1, 2, 2, 1)));

        when(fetcher.call(2)).thenAnswer((_) => new Future.error("doh"));
      });

      group('resultStream', () {
        Future<List<SearchResult>> resultsFuture;

        setUp(() {
          resultsFuture = paged.all.resultStream.toList();
        });

        test('completes with exception', () {
          expect(resultsFuture, throws);
        });

        group('completes with', () {
          var exception;

          setUp(() async {
            try {
              await resultsFuture;
            } catch (e) {
              exception = e;
            }
          });

          test('calls fetcher with page 1', () {
            verify(fetcher.call(1)).called(1);
            verify(fetcher.call(2)).called(1);
            verifyNoMoreInteractions(fetcher);
          });

          test('expected error', () {
            expect(exception, equals("doh"));
          });
        });
      });

      group('percentComplete', () {
        Future<List<PercentComplete>> futureValues;

        setUp(() {
          futureValues = paged.all.percentComplete.take(2).toList();
        });

        test('completes', () {
          expect(futureValues, completes);
        });

        test('starts at 0/10 and remains there', () {
          expect(futureValues, completion(orderedEquals([
            new PercentComplete().setTotal(10.0),
            new PercentComplete().setTotal(2.0).addToCurrent(1.0)
          ])));
        });
      });
    });
  });
}
