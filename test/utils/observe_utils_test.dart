library observe.utils.test;

import 'package:observe/observe.dart';
import 'package:test/test.dart';
import 'package:frentity/src/utils/observe_utils.dart';
import 'observe_test_utils.dart';
import 'package:frappe/frappe.dart';
import 'dart:async';
import 'package:frentity/src/utils/frappe_utils.dart';

main() {
  group('derivedObservableList', () {
    ObservableList<String> observedList;
    ObservableList<FooString> derivedList;

    group('when observed list starts empty', () {
      setUp(() {
        observedList = new ObservableList();
        derivedList =
            derivedObservableList(observedList, (String s) => new FooString(s));
      });

      test('the derived list starts empty', () {
        expect(derivedList, isEmpty);
      });

      group('and a single item is added', () {
        setUp(() {
          observedList.add('first');
          return spin(1);
        });

        test('the derived list now has one item', () {
          expect(derivedList, hasLength(1));
        });

        test('the derived list now has expected item', () {
          expect(derivedList.first.foo, equals('first'));
        });
      });

      group('and two items are added', () {
        setUp(() {
          observedList.addAll(['first', 'second']);
          return spin(1);
        });

        test('the derived list now has one item', () {
          expect(derivedList, hasLength(2));
        });

        test('the derived list now has expected item', () {
          expect(derivedList.map((f) => f.foo),
              orderedEquals(['first', 'second']));
        });
      });
    });

    group('when observed list starts with three items', () {
      final initialItems = ['first', 'second', 'third'];

      setUp(() {
        observedList = new ObservableList.from(initialItems);
        derivedList =
            derivedObservableList(observedList, (String s) => new FooString(s));
        return spin(1);
      });

      test('the derived list starts with three items', () {
        expect(derivedList, hasLength(3));
      });

      test('the derived list starts with thee expected items', () {
        expect(derivedList.map((f) => f.foo), orderedEquals(initialItems));
      });

      group('and a single item is deleted', () {
        setUp(() {
          observedList.remove('second');
          return spin(1);
        });

        test('the derived list has two items', () {
          expect(derivedList, hasLength(2));
        });

        test('the derived list has expected items', () {
          expect(
              derivedList.map((f) => f.foo), orderedEquals(['first', 'third']));
        });
      });

      group('and two items are deleted', () {
        setUp(() {
          observedList.remove('second');
          observedList.removeAt(0);
          return spin(1);
        });

        test('the derived list has one items', () {
          expect(derivedList, hasLength(1));
        });

        test('the derived list has expected items', () {
          expect(derivedList.map((f) => f.foo), orderedEquals(['third']));
        });
      });

      group('and a single item is updated', () {
        setUp(() {
          observedList[1] = 'newSecond';
          return spin(1);
        });

        test('the derived list has three items', () {
          expect(derivedList, hasLength(3));
        });

        test('the derived list has expected items', () {
          expect(derivedList.map((f) => f.foo),
              orderedEquals(['first', 'newSecond', 'third']));
        });
      });

      group('and two items are updated', () {
        setUp(() {
          observedList[1] = 'newSecond';
          observedList[2] = 'newThird';
          return spin(1);
        });

        test('the derived list has three items', () {
          expect(derivedList, hasLength(3));
        });

        test('the derived list has expected items', () {
          expect(derivedList.map((f) => f.foo),
              orderedEquals(['first', 'newSecond', 'newThird']));
        });
      });

      group('and two outer items are updated', () {
        setUp(() {
          observedList[0] = 'newFirst';
          observedList[2] = 'newThird';
          return spin(1);
        });

        test('the derived list has three items', () {
          expect(derivedList, hasLength(3));
        });

        test('the derived list has expected items', () {
          expect(derivedList.map((f) => f.foo),
              orderedEquals(['newFirst', 'second', 'newThird']));
        });
      });

      group('and a single item is moved', () {
        setUp(() {
          final v = observedList[0];
          observedList[0] = observedList[2];
          observedList[2] = v;
          return spin(1);
        });

        test('the derived list has three items', () {
          expect(derivedList, hasLength(3));
        });

        test('the derived list has expected items', () {
          expect(derivedList.map((f) => f.foo),
              orderedEquals(['third', 'second', 'first']));
        });
      });
    });
  });

  group('aggregatedObservableList', () {
    ObservableList<String> observedList1;
    ObservableList<String> observedList2;
    ObservableList<String> aggregatedList;

    group('when observed list starts empty', () {
      setUp(() {
        observedList1 = new ObservableList();
        observedList2 = new ObservableList();
        aggregatedList =
            aggregatedObservableList([observedList1, observedList2]);
      });

      test('the derived list starts empty', () {
        expect(aggregatedList, isEmpty);
      });

      group('and a single item is added to first list', () {
        setUp(() {
          observedList1.add('first');
          return spin(1);
        });

        test('the aggregated list now has one item', () {
          expect(aggregatedList, hasLength(1));
        });

        test('the aggregated list now has expected item', () {
          expect(aggregatedList.first, equals('first'));
        });
      });

      group('and a single item is added to second list', () {
        setUp(() {
          observedList2.add('second');
          return spin(1);
        });

        test('the aggregated list now has one item', () {
          expect(aggregatedList, hasLength(1));
        });

        test('the aggregated list now has expected item', () {
          expect(aggregatedList.first, equals('second'));
        });
      });

      group('and two items are added to each list', () {
        final expectedItems = ['first', 'first-2', 'second', 'second-2'];
        setUp(() async {
          observedList2.add('second');
          await spin(1);
          observedList1.add('first');
          await spin(1);
          observedList1.add('first-2');
          await spin(1);
          observedList2.add('second-2');
          return spin(1);
        });

        test('the aggregated list now has four items', () {
          expect(aggregatedList, hasLength(4));
        });

        test('the aggregated list now has expected items', () {
          expect(aggregatedList, unorderedEquals(expectedItems));
        });

        test('the aggregated list now has expected items in correct order', () {
          expect(aggregatedList, orderedEquals(expectedItems));
        });
      });
    });

    group('when each observed list starts with two items', () {
      final initialItems1 = ['first', 'second'];
      final initialItems2 = ['third', 'fourth'];

      setUp(() {
        observedList1 = new ObservableList.from(initialItems1);
        observedList2 = new ObservableList.from(initialItems2);
        aggregatedList =
            aggregatedObservableList([observedList1, observedList2]);
        return spin(1);
      });

      test('the aggregated list starts with three items', () {
        expect(aggregatedList, hasLength(4));
      });

      test('the aggregated list starts with thee expected items', () {
        expect(aggregatedList,
            orderedEquals(['first', 'second', 'third', 'fourth']));
      });

      group('and a single item is deleted from each list', () {
        setUp(() {
          observedList1.remove('second');
          observedList2.remove('third');
          return spin(1);
        });

        test('the aggregated list has two items', () {
          expect(aggregatedList, hasLength(2));
        });

        test('the aggregated list has expected items', () {
          expect(aggregatedList, orderedEquals(['first', 'fourth']));
        });
      });
    });

    group('when list of observed lists is itself observable', () {
      ObservableList listOfLists;

      setUp(() {
        observedList1 = new ObservableList();
        observedList2 = new ObservableList();
        listOfLists = new ObservableList();
        aggregatedList = aggregatedObservableList(listOfLists);
      });

      test('the derived list starts empty', () {
        expect(aggregatedList, isEmpty);
      });

      group('and a single observable list is added and two issues', () {
        setUp(() {
          listOfLists.add(observedList1);
          observedList1.add('first');
          observedList1.add('firstb');
          return spin(1);
        });

        test('the aggregated list now has one item', () {
          expect(aggregatedList, hasLength(2));
        });

        test('the aggregated list now has expected items', () {
          expect(aggregatedList, orderedEquals(['first', 'firstb']));
        });

        group('and a second observable list is added and one issue', () {
          setUp(() async {
            listOfLists.add(observedList2);
            await spin(5);
            observedList2.add('second');
            return spin(1);
          });

          test('the aggregated list now has three items', () {
            expect(aggregatedList, hasLength(3));
          });

          test('the aggregated list now has expected items', () {
            expect(
                aggregatedList, orderedEquals(['first', 'firstb', 'second']));
          });

          group('and then first list is removed again', () {
            setUp(() async {
              listOfLists.remove(observedList1);
              await spin(10);
            });

            test('the derived list has only one item', () {
              expect(aggregatedList, hasLength(1));
            });

            test('the derived list has expected item', () {
              expect(aggregatedList, equals(['second']));
            });

            test('further additions to removed list ignored', () async {
              observedList1.add('oneagain');
              await spin(1);
              expect(aggregatedList, equals(['second']));
            });
          });

          group('and then second list is removed again', () {
            setUp(() async {
              listOfLists.remove(observedList2);
              await spin(10);
            });

            test('the derived list has only one item', () {
              expect(aggregatedList, hasLength(2));
            });

            test('the derived list has expected item', () {
              expect(aggregatedList, equals(['first', 'firstb']));
            });

            test('further additions to removed list ignored', () async {
              observedList2.add('oneagain');
              await spin(1);
              expect(aggregatedList, equals(['first', 'firstb']));
            });
          });
        });
      });
    });
  });

  group('mergeEventStreamsFromObservable', () {
    ControllableEventStream esController1;
    ControllableEventStream esController2;
    ControllableEventStream esController3;
    ObservableList<EventStream> eses;
    EventStream combinedEventStream;
    List<String> receivedEvents;

    setUp(() {
      esController1 = new ControllableEventStream.std();
      esController2 = new ControllableEventStream.std();
      esController3 = new ControllableEventStream.std();
      eses = new ObservableList.from([
        esController1.eventStream,
        esController2.eventStream,
        esController3.eventStream
      ]);

      combinedEventStream = mergeEventStreamsFromObservable(eses);

      receivedEvents = [];
      combinedEventStream.listen((e) => receivedEvents.add(e));
      return spin(5);
    });

    group('when event added to first stream', () {
      setUp(() {
        esController1.controller.add('first');
        return spin(5);
      });

      test('a single event is received', () {
        expect(receivedEvents, hasLength(1));
      });

      test('the expected event is received', () {
        expect(receivedEvents.first, equals('first'));
      });
    });

    group('when event added to second stream', () {
      setUp(() {
        esController2.controller.add('second');
        return spin(5);
      });

      test('a single event is received', () {
        expect(receivedEvents, hasLength(1));
      });

      test('the expected event is received', () {
        expect(receivedEvents.first, equals('second'));
      });
    });

    group('when event added to all streams', () {
      setUp(() {
        esController1.controller.add('first');
        esController2.controller.add('second');
        esController3.controller.add('third');
        return spin(5);
      });

      test('three events are received', () {
        expect(receivedEvents, hasLength(3));
      });

      test('the expected events are received', () {
        expect(receivedEvents, unorderedEquals(['first', 'second', 'third']));
      });
    });

    group('when event added to all streams then second stream removed', () {
      setUp(() async {
        esController1.controller.add('first');
        await spin(5);
        esController2.controller.add('second');
        await spin(5);
        esController3.controller.add('third');
        eses.removeAt(1);
        esController2.controller.add('second-2');
        await spin(5);
        esController2.controller.add('second-3');
        return spin(5);
      });

      test('three events are received', () {
        expect(receivedEvents, hasLength(3));
      });

      test('the expected events are received', () {
        expect(receivedEvents, unorderedEquals(['first', 'second', 'third']));
      });
    });
  });

  group('observableListToPropertyList', () {
    ObservableList<String> listOfString;
    Property<List<String>> propertyList;
    List<List<String>> receivedEvents;

    setUp(() {
      listOfString = new ObservableList();

      propertyList = observableListToPropertyList(listOfString);

//      propertyList.listen(print);

      receivedEvents = [];
      propertyList.listen((e) => receivedEvents.add(e));
      return spin(5);
    });

    group('when event added to first stream', () {
      setUp(() async {
        receivedEvents = [];
        listOfString.add('first');
        return spin(10)
            .then((_) => new Future.delayed(const Duration(milliseconds: 1)));
      });

      test('one event is received', () {
        expect(receivedEvents, hasLength(1));
      });

      test('the expected first event is received', () {
        expect(receivedEvents.first, equals(['first']));
      });

      group('and then other two streams added', () {
        setUp(() async {
          receivedEvents.clear();
          listOfString.add('second');
          listOfString.add('third');
          return spin(10)
              .then((_) => new Future.delayed(const Duration(milliseconds: 1)));
        });

        test('a single event is received', () {
          expect(receivedEvents, hasLength(1));
        });

        test('the expected event is received', () {
          expect(receivedEvents.first,
              orderedEquals(['first', 'second', 'third']));
        });
      });
    });
  });
}

class FooString {
  final String foo;

  FooString(this.foo);
}
