library frappe.test.utils;

import 'package:frappe/frappe.dart';

ReactableEventCollector snoop(Reactable reactable) =>
    new ReactableEventCollector(reactable);

class ReactableEventCollector<T> {
  final List<T> received = [];
  void clear() => received.clear();

  ReactableEventCollector(Reactable<T> reactable) {
    reactable.listen(received.add);
  }
}

listen(Reactable r, String l) => r.listen((v) => print('$l: $v'));
