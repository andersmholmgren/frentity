library frappe.utils.test;

import 'package:test/test.dart';
import 'observe_test_utils.dart';
import 'package:frappe/frappe.dart';
import 'dart:async';
import 'package:frentity/src/utils/frappe_utils.dart';

main() {
  group('iterablePropertiesToPropertyIterable', () {
    ControllableProperty<String> cProperty1;
    ControllableProperty<String> cProperty2;
    ControllableProperty<String> cProperty3;
    List<Property<String>> listOfProperty;
    Property<List<String>> propertyList;
    List<List<String>> receivedEvents;

    setUp(() {
      cProperty1 = new ControllableProperty('first');
      cProperty2 = new ControllableProperty();
      cProperty3 = new ControllableProperty();

      listOfProperty = [
        cProperty1.property,
        cProperty2.property,
        cProperty3.property
      ];

      propertyList = iterablePropertiesToPropertyIterable(listOfProperty);
//        propertyList = Reactable.collect(listOfProperty);

      receivedEvents = [];
      propertyList.listen((e) => receivedEvents.add(e));
//        cProperty1.property.listen((v) => print('p1: $v'));
//        cProperty2.property.listen((v) => print('p2: $v'));
//        cProperty3.property.listen((v) => print('p3: $v'));
//        propertyList.listen((v) => print('plist: $v'));
      return spin(5);
    });

    // note: as properties have not initial value must add to all streams
    group('when event added to each stream', () {
      setUp(() {
        cProperty1.value = 'first';
        cProperty2.value = 'second';
        cProperty3.value = 'third';
        return spin(10)
            .then((_) => new Future.delayed(const Duration(milliseconds: 1)));
      });

      test('a single event is received', () {
        expect(receivedEvents, hasLength(1));
      });

      test('the expected event is received', () {
        expect(
            receivedEvents.first, orderedEquals(['first', 'second', 'third']));
      });

      group('and then event added to second stream', () {
        setUp(() {
          receivedEvents.clear();
          cProperty2.value = 'secondb';
          return spin(10)
              .then((_) => new Future.delayed(const Duration(milliseconds: 1)));
        });

        test('a single event is received', () {
          expect(receivedEvents, hasLength(1));
        });

        test('the expected event is received', () {
          expect(receivedEvents.first,
              orderedEquals(['first', 'secondb', 'third']));
        });

        group('and then event added to first stream', () {
          setUp(() {
            receivedEvents.clear();
            cProperty1.value = 'firstb';
            return spin(10).then(
                (_) => new Future.delayed(const Duration(milliseconds: 1)));
          });

          test('a single event is received', () {
            expect(receivedEvents, hasLength(1));
          });

          test('the expected event is received', () {
            expect(receivedEvents.first,
                orderedEquals(['firstb', 'secondb', 'third']));
          });
        });
      });
    });
  });

  group('mapPropertyIterable', () {
    DerivedResource derivedResource;
    Property<List<String>> mappedPropertyList;
    List<List<String>> receivedEvents;

    setUp(() async {
      derivedResource = new DerivedResource();
//      derivedResource.simples.listen(print);

      mappedPropertyList = mapPropertyIterable(
          derivedResource.simples, (SimpleResource f) => f.property);

      receivedEvents = [];

      mappedPropertyList.listen((v) {
        receivedEvents.add(v);
//        print('mappedPropertyList $v');
      });
      await new Future.delayed(const Duration(milliseconds: 1));
    });

    group('when one resource added', () {
      SimpleResource simple1;

      setUp(() async {
        simple1 = new SimpleResource('1');
        derivedResource.setSimpleValues([simple1]);
        await new Future.delayed(const Duration(milliseconds: 5));
      });

      test('a single event is received', () {
        expect(receivedEvents, hasLength(1));
      });

      test('expected event is received', () {
        expect(receivedEvents.first, orderedEquals(['1']));
      });

      group('and then the resource value changed', () {
        setUp(() async {
          receivedEvents = [];
          simple1.value = '1b';
          await new Future.delayed(const Duration(milliseconds: 1));
        });

        test('a single event is received', () {
          expect(receivedEvents, hasLength(1));
        });

        test('expected event is received', () {
          expect(receivedEvents.first, orderedEquals(['1b']));
        });
      });

      group('and then a second resource added', () {
        SimpleResource simple2;
        setUp(() async {
          receivedEvents = [];
          simple2 = new SimpleResource('2');
          derivedResource.setSimpleValues([simple1, simple2]);
          await new Future.delayed(const Duration(milliseconds: 1));
        });

        test('a single event is received', () {
          expect(receivedEvents, hasLength(1));
        });

        test('expected event is received', () {
          expect(receivedEvents.first, orderedEquals(['1', '2']));
        });

        group('and then both resource values changed', () {
          setUp(() async {
            receivedEvents = [];
            simple1.value = '1b';
            simple2.value = '2b';
            await new Future.delayed(const Duration(milliseconds: 1));
          });

          test('two events is received', () {
            expect(receivedEvents, hasLength(2));
          });

          test('expected first event is received', () {
            expect(receivedEvents.first, orderedEquals(['1', '2b']));
          });

          test('expected second event is received', () {
            expect(receivedEvents.elementAt(1), orderedEquals(['1b', '2b']));
          });
        });
      });
    });
  });
}

class SimpleResource {
  final ControllableProperty<String> _cProperty;
  Property<String> get property => _cProperty.property;

  set value(String v) {
//    print('----------------');
//    print('set value $v');
    _cProperty.value = v;
  }

  SimpleResource([String value])
      : this._cProperty = new ControllableProperty(value);

  String toString() => _cProperty.property.toString();
}

class DerivedResource {
  ControllableProperty<List<SimpleResource>> _simpleList =
      new ControllableProperty();

  void setSimpleValues(Iterable<SimpleResource> values) {
    _simpleList.value = values;
  }

  Property<List<SimpleResource>> get simples => _simpleList.property;
}
