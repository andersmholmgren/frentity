library observe.test.utils;

import 'dart:async';
import 'package:observe/observe.dart';

typedef bool Predicate(); // TODO: take an arg an work with matcher predicate

Future spin([int count = 10]) => new DirtyObservableSpinner(count, () => true,
    failOnUnsatisfiedPredicate: false).spin();

class DirtyObservableSpinner {
  final int numSpins;
  final Predicate predicate;
  final bool failOnUnsatisfiedPredicate;

  DirtyObservableSpinner(this.numSpins, this.predicate,
      {this.failOnUnsatisfiedPredicate: true});

  Future spin() {
    int count = 0;

    final f = Future.doWhile(() {
//      print('spin $count');
      Observable.dirtyCheck();
      return new Future.value(count++ < numSpins && predicate());
    });

    return f.then((_) {
      if (count >= numSpins && failOnUnsatisfiedPredicate) {
        throw 'predicate not satisfied after $count spins';
      }
      return true;
    });
  }
}

StreamEventCollector snoopStream(Stream stream) =>
    new StreamEventCollector(stream);

class StreamEventCollector<T> {
  final List<T> received = [];
  void clear() => received.clear();

  StreamEventCollector(Stream<T> stream) {
    stream.listen(received.add);
  }
}
