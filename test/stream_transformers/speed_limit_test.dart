library debounce_test;

import 'dart:async';
import 'package:guinness/guinness.dart';
import 'package:frentity/src/stream_controllers/speed_limit.dart';
import 'util.dart';

void main() => describe("Speedlimit", () {
  describe("with single subscription stream", () {
    testWithStreamController(() => new StreamController());
  });

  describe("with broadcast stream", () {
    testWithStreamController(() => new StreamController.broadcast());
  });
});

const int speedLimitMillis = 50;

expectDelayInBetween(
    TimeStamped<TimeStamped<int>> first, int lowerBound, int upperBound) {
  expect(first.timestamp.difference(first.value.timestamp).inMilliseconds).not
      .toBeLessThan(lowerBound);
  expect(first.timestamp.difference(first.value.timestamp).inMilliseconds).not
      .toBeGreaterThan(upperBound);
}

void testWithStreamController(StreamController provider()) {
  StreamController controller;
  Duration duration;

  beforeEach(() {
    controller = provider();
    duration = new Duration(milliseconds: speedLimitMillis);
  });

  afterEach(() {
    controller.close();
  });

  it("maintains the given minimum duration between events", () {
    return testStream(controller.stream.transform(new SpeedLimit(duration)),
        behavior: () async {
      controller..add(timeStamp(1))..add(timeStamp(2));
      await delay(speedLimitMillis ~/ 2);
      controller.add(timeStamp(3));
      await delay((speedLimitMillis * 1.5).toInt());
      controller.add(timeStamp(4));
      return new Future.delayed(duration * 3, () => true);
    }, expectation: (values) {
      expect(values.map((v) => v.value).map((v) => v.value))
          .toEqual([1, 2, 3, 4]);
      expectDelayInBetween(values.first, 0, 10);
      expectDelayInBetween(
          values.elementAt(1), speedLimitMillis - 2, speedLimitMillis + 10);
      expectDelayInBetween(values.elementAt(2), (speedLimitMillis ~/ 2) - 2,
          speedLimitMillis ~/ 2 + 10);
      expectDelayInBetween(values.elementAt(3), 0, 10);
    }, valueConverter: timeStamp);
  });

  it("closes transformed stream when source stream is done", () {
    var stream = controller.stream.transform(new SpeedLimit(duration));
    var result = stream.toList();
    controller
      ..add(1)
      ..close();
    return result.then((values) {
      expect(values).toEqual([1]);
    });
  });

  it("cancels input stream when transformed stream is cancelled", () {
    var completerA = new Completer();
    var controller = new StreamController(onCancel: completerA.complete);

    return testStream(controller.stream.transform(new SpeedLimit(duration)),
        expectation: (_) => completerA.future);
  });

  it("doesn't speedlimit errors", () {
    return testErrorsAreForwarded(
        controller.stream.transform(new SpeedLimit(duration)), behavior: () {
      controller
        ..addError(1)
        ..addError(2)
        ..addError(3)
        ..close();
    }, expectation: (errors) => expect(errors).toEqual([1, 2, 3]));
  });

  it("returns a stream of the same type", () {
    var stream = controller.stream.transform(new SpeedLimit(duration));
    expect(stream.isBroadcast).toBe(controller.stream.isBroadcast);
  });
}
