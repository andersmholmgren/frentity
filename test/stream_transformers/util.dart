library test_util;

import 'dart:async';

Function doNothing = (_) {};

typedef Converter(v);

_noopConverter(v) => v;

TimeStamped timeStamp(v) => new TimeStamped(v);

Future testStream(Stream stream, {behavior(), expectation(List values),
    Converter valueConverter: _noopConverter}) {
  var results = [];

  var subscription = stream.listen((value) {
    results.add(valueConverter(value));
  });

  return new Future(() {
    if (behavior != null) {
      return behavior();
    }
  }).then((_) => new Future(() {
    subscription.cancel();
  })).then((_) => expectation(results));
}

Future testErrorsAreForwarded(Stream stream,
    {behavior(), expectation(List errors)}) {
  var errors = [];
  return testStream(stream.handleError((e) => errors.add(e)),
      behavior: behavior, expectation: (_) => expectation(errors));
}

class TimeStamped<T> {
  final T value;
  final DateTime timestamp;

  TimeStamped(this.value) : timestamp = new DateTime.now();
}

Future delay([int milliseconds = 1]) =>
    new Future.delayed(new Duration(milliseconds: milliseconds));
