library actions.executors.test;

import 'package:test/test.dart';
import 'package:logging/logging.dart';
import 'dart:async';
import 'package:frentity/src/actions/executors_impl.dart';
import 'package:frentity/src/actions/action_impl.dart';
import 'package:frentity/src/actions/action.dart';

main() {
  Logger.root.level = Level.SHOUT;
  Logger.root.onRecord.listen(print);

  SerialActionExecutor executor;

  setUp(() {
    executor = new SerialActionExecutor.simple();
  });

  group('execute', () {
    SimpleAction _testSuccessAction;

    setUp(() {
      _testSuccessAction = new SimpleAction(() {
        return new Future.value('the result');
      }, 'hi');
    });

    group('when task completes successfully', () {
      test('completes', () async {
        expect(await executor.execute(_testSuccessAction),
            new isInstanceOf<CompletedAction>());
      });

      test('completes successfully', () async {
        expect((await executor.execute(_testSuccessAction)).result,
            new isInstanceOf<ActionSuccess>());
      });

      test('completes with expected result', () async {
        ActionSuccess success =
            (await executor.execute(_testSuccessAction)).result;
        expect(success.result, equals('the result'));
      });

      test('completes with action start before or equal to end', () async {
        ActionSuccess success =
            (await executor.execute(_testSuccessAction)).result;
        expect(success.actionStart.isAfter(success.actionEnd), equals(false));
      });

      test('completes with expected action', () async {
        expect((await executor.execute(_testSuccessAction)).action,
            equals(_testSuccessAction));
      });

      test('action completionFuture completes with correct result', () async {
        executor.execute(_testSuccessAction);
        expect(await _testSuccessAction.completionFuture, equals('the result'));
      });
    });
  });

  group('when task completes with error', () {
    SimpleAction _testFailAction;

    setUp(() {
      _testFailAction = new SimpleAction(() async {
        return new Future.error('woops');
      }, 'hi');
    });

    _swallowError() async {
      try {
        await _testFailAction.completionFuture;
      } catch (e) {}
    }
    test('completes', () async {
      _swallowError();
      expect(await executor.execute(_testFailAction),
          new isInstanceOf<CompletedAction>());
    });

    test('completes unsuccessfully', () async {
      _swallowError();
      expect((await executor.execute(_testFailAction)).result,
          new isInstanceOf<ActionFailure>());
    });

    test('completes with expected message', () async {
      _swallowError();
      ActionFailure failure = (await executor.execute(_testFailAction)).result;
      expect(failure.message, equals('woops'));
    });

    test('completes with action start before or equal to end', () async {
      _swallowError();
      ActionFailure failure = (await executor.execute(_testFailAction)).result;
      expect(failure.actionStart.isAfter(failure.actionEnd), equals(false));
    });

    test('completes with expected action', () async {
      _swallowError();
      expect((await executor.execute(_testFailAction)).action,
          equals(_testFailAction));
    });

    test('action completionFuture throws', () async {
      executor.execute(_testFailAction);
      expect(_testFailAction.completionFuture, throws);
    });
  });

  // TODO: multiple actions
}
