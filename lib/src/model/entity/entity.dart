library gissue_common.model.entity;

import 'package:frappe/frappe.dart';
import 'dart:async';
import 'package:observe/observe.dart';
import 'package:frentity/src/actions/action.dart';

// exposes an observable property
// communicates with backend
// responds to updates from back end for both
// - responses to requests
// - websockets in the future
// is the front door to the entity

abstract class Entity<T> implements CompletedActionSource {
  Observable get value;

  Property<bool> get isActive;

  /// indicates whether the entity needs to be refetched when it is activated
  Property<bool> get isStale;

  Property<EntityLoadState> get loadState;

  @deprecated // try to be more reactive
  Future load({bool reload: false});

  @override
  EventStream<CompletedAction> get completedActions;
}

abstract class MutableEntity<T> extends Entity<T> {
  Future apply(Action action);
}

//enum EntityState {
//  INACTIVE, LOADING, READY
//}

enum EntityLoadState { INIT, LOADING, LOADED }

class EntityActionMetaData {
  final String entityName;
  final String entityAction;
  final String entityId;

  EntityActionMetaData(this.entityName, this.entityAction, this.entityId);

  static const String entityActionCreate = 'create';
  static const String entityActionSearch = 'search';
  static const String entityActionFetch = 'fetch';
  static const String entityActionUpdate = 'udpate';
  static const String entityActionDelete = 'delete';
}

abstract class EntityAction<T> extends Action<T> {
  EntityActionMetaData get metaData;
}
