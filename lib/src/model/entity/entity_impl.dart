library frentity.model.entity.impl;

import 'package:frappe/frappe.dart';
import 'dart:async';
import 'entity.dart';
import 'package:frentity/src/actions/action.dart';
import 'package:frentity/src/actions/executors.dart';
import 'package:frentity/src/actions/executors_impl.dart';
import 'package:frentity/src/actions/action_impl.dart';
import 'package:frentity/src/utils/frappe_utils.dart';
import 'package:logging/logging.dart';

Logger _log = new Logger('frentity.model.entity.impl');

abstract class BaseEntity<T> implements MutableEntity<T> {
  final ControllableEventStream<CompletedAction> _completedActionStream =
      new ControllableEventStream<CompletedAction>.std();

  EventStream<CompletedAction> get completedActions =>
      _completedActionStream.eventStream;

  final ActionExecutor _executor;

  final Property<bool> isActive;

  final Property<bool> isStale;

  final ControllableEventStream _reloadRequestedStream;

  final ControllableEventStream _loadedStream;

  final EventStream _reloadStream;

  final Property<EntityLoadState> loadState;

  @deprecated // if we need this then it should be a Property<bool>
  bool _isLoaded = false;

  BaseEntity._(
      this.isActive,
      this.isStale,
      this._reloadRequestedStream,
      EventStream reloadStream,
      ControllableEventStream loadedStream,
      this._executor)
      : this._reloadStream = reloadStream,
        this._loadedStream = loadedStream,
        this.loadState =
            _createLoadState(reloadStream, loadedStream.eventStream) {
//    this.isActive.distinct().listen((b) => print('$_id isActive: $b'));
    this.isStale.distinct().listen((b) => print('$_id isStale: $b'));
//    _reloadRequestedStream.eventStream.distinct().listen((_) => print('$runtimeType: _reloadRequestedStream'));
//    this.isActive.and(this.isStale).distinct().listen((b) => print('$runtimeType: isActive.and(isStale): $b'));

    // TODO: if this is removed then it busts deep linking, presumably cause the
    // LOADED state is not remembered by the frappe stream. Want to remove
//    loadState.listen((b) => print('$_id : $b'));

    _reloadStream.listen((_) {
      print('$_id reload');
      _load();
    });

//    completedActions.listen((b) => print('$_id : $b'));
  }

  String get _id => '$runtimeType (${identityHashCode(this)}): ';

  BaseEntity._1(Property<bool> isActive, Property<bool> isStale,
      ControllableEventStream reloadRequestedStream,
      ControllableEventStream loadedStream, ActionExecutor executor)
      : this._(isActive, isStale, reloadRequestedStream,
          _createReloadStream(isActive, isStale, reloadRequestedStream),
          loadedStream, executor);

  BaseEntity._2(Property<bool> isActive, Stream dirtiedStream,
      ControllableEventStream loadedStream, ActionExecutor _executor,
      bool startFresh)
      : this._1(isActive,
          _createIsStale(loadedStream.eventStream, dirtiedStream, startFresh),
          new ControllableEventStream.std(), loadedStream, _executor);

  BaseEntity(Property<bool> isActive, Stream dirtied, ActionExecutor executor,
      {bool startFresh: false})
      : this._2(isActive.distinct(), dirtied, new ControllableEventStream.std(),
          executor, startFresh);

  BaseEntity.serial(Property<bool> isActive, Stream dirtied,
      {bool startFresh: false})
      : this(isActive, dirtied, new SerialActionExecutor.simple(),
          startFresh: startFresh);

  static Property<bool> _createIsStale(
      EventStream loaded, Stream dirtied, bool startFresh) {
    // stale whenever we get a new event on the dirtied stream and clean
    // when ever we get a successful load event

//    loaded.listen((v) => print('loaded $v'));
//    dirtied.listen((v) => print('dirtied $v'));
//    loaded.map((_) => false).listen((v) => print('loaded mapped $v'));
//    dirtied.map((_) => true).listen((v) => print('dirtied mapped $v'));

    final isStaleStream =
        loaded.map((_) => false).merge(dirtied.map((_) => true));

//    isStaleStream.listen((v) => print('isStaleStream $v'));

    return new Property.fromStreamWithInitialValue(!startFresh, isStaleStream)
        .distinct();
  }

  @override
  Future apply(Action action) {
    Action _maybeWrapAction() {
      if (action is UndoableAction) {
        final undo = action.undoAction;
        final wrappedTask = () {
          apply(undo);
          return undo.completionFuture;
        };
        final wrappedUndo =
            new SimpleAction(wrappedTask, undo.completionMessage);
        return new DefaultUndoableAction(action, wrappedUndo);
      }
      return action;
    }

    var executingAction = _maybeWrapAction();
    _executor.execute(executingAction).then((result) {
//      print('$runtimeType: completed action $result');
      _completedActionStream.controller.add(result);
      return result;
    });

    return executingAction.completionFuture;
  }

  Future _reload() {
//    print('$runtimeType (${identityHashCode(this)}) _reload');
    _reloadRequestedStream.controller.add(true);
    return _loadedStream.eventStream.first;
  }

  @override
  @deprecated
  Future load({bool reload: false}) {
    return !_isLoaded || reload ? this._reload() : new Future.value();
  }

  Future _load() => createLoadAction().then((loadAction) {
    // use the action future as the return because that will reflect whether
    // an exception is thrown and returns the result a caller would more likely
    // expect
    apply(loadAction);

    return loadAction.completionFuture.then((_) {
      _isLoaded = true;
      _loadedStream.controller.add(null);
    });
  });

//  BaseAction<T> get loadAction;

  Future<BaseAction<T>> createLoadAction();
}

Property<EntityLoadState> _createLoadState(
    EventStream reloadStream, EventStream loadedStream) {
  final loading = reloadStream.map((_) => EntityLoadState.LOADING);
  final loaded = loadedStream.map((_) => EntityLoadState.LOADED);

//  loading.listen((v) => print('loading $v'));
//  loaded.listen((v) => print('loaded $v'));

  final loadState = new Property.fromStreamWithInitialValue(
      EntityLoadState.INIT, loaded.merge(loading));

  // TODO: We seem to need a subscriber on this immediately
  loadState.listen((_) => null);

//  loadState.listen((v) => print('loadState: $v'));
  return loadState;
}

//// defining this in terms of just these properties. A bit odd but as long
//// as the reload logic is sound we should be good
//Property<EntityLoadState> _createLoadState(EventStream reloadStream, EventStream eventStream) {
//  final active = isActive.distinct().asProperty();
//  final stale = isStale.distinct().asProperty();
//
//  return active.combine(stale, (a, d) {
//    if (!a) {
//      return EntityLoadState.INIT;
//    }
//    return a && d ? EntityLoadState.LOADING : EntityLoadState.LOADED;
//  });
//}

EventStream _createReloadStream(Property<bool> isActive, Property<bool> isStale,
    ControllableEventStream reloadRequestedStream) {
//  return isActive.and(isStale).where((b) => b).asStream()
//      .distinct().asBroadcastStream()
//      .merge(reloadRequestedStream.eventStream);

//  isActive.listen((v) => print('- isActive $v'));
//  isStale.listen((v) => print('- isStale $v'));
//
//  isActive.and(isStale).where((b) => b).listen((v) => print('aAndD $v'));
//
//  reloadRequestedStream.eventStream
//      .listen((v) => print('reloadRequestedStream $v'));

  // can't have distinct as want to catch all reloads not just the first
  final result = isActive
      .and(isStale)
      .where((b) => b)
      .asEventStream()
      .merge(reloadRequestedStream.eventStream);

//  result.listen((v) => print('result $v'));

  return result;
}

EventStream<CompletedAction> createMergedParentChildrenCompletedActions(
        CompletedActionSource currentEntity,
        Property<List<CompletedActionSource>> childEntities) =>
    createMergedParentChildrenCompletedActions2(
        currentEntity.completedActions, childEntities);

EventStream<CompletedAction> createMergedParentChildrenCompletedActions2(
    EventStream<CompletedAction> parentCompletedActions,
    Property<List<CompletedActionSource>> childEntities) {
  final EventStream<CompletedAction> childEntityCompletedActions = childEntities
      .flatMapLatest((List<CompletedActionSource> children) {
    final EventStream<CompletedAction> merged = children
        .map((child) => child.completedActions)
        .fold(null, (EventStream<CompletedAction> c,
            EventStream<CompletedAction> e) => c != null ? c.merge(e) : e);

    return merged != null
        ? merged
        : new EventStream(new Stream.fromIterable([]));
  }).asEventStream();

  final combinedCompletedActions =
      childEntityCompletedActions.merge(parentCompletedActions);

//  childEntityCompletedActions.listen((v) => print('childEntityCompletedActions $v'));
//  parentCompletedActions.listen((v) => print('parentCompletedActions $v'));
//  combinedCompletedActions.listen((v) => print('combinedCompletedActions $v'));

  return combinedCompletedActions;
}

class SimpleEntityAction<T> extends SimpleAction<T> implements EntityAction<T> {
  final EntityActionMetaData metaData;

  SimpleEntityAction(Future<T> task(), String completionMessage, this.metaData)
      : super(task, completionMessage);
}

class SimpleEntityLoadAction<T> extends SimpleEntityAction<T>
    implements LoadAction<T> {
  SimpleEntityLoadAction(
      Future<T> task(), String completionMessage, EntityActionMetaData metaData)
      : super(task, completionMessage, metaData);

  SimpleEntityLoadAction.def(
      Future<T> task(), String entityDescription, EntityActionMetaData metaData)
      : super(task, '$entityDescription loaded', metaData);
}

abstract class DefaultEntityAction<T> extends DefaultAction<T>
    implements EntityAction<T> {
  final EntityActionMetaData metaData;

  DefaultEntityAction(this.metaData);
}
