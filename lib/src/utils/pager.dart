library gissue.common.rest.pager;

import 'dart:async';
import 'package:hateoas_models/hateoas_models.dart';
import 'package:option/option.dart';
import 'package:frentity/frentity.dart';
import 'package:frappe/frappe.dart';

typedef Future<SearchResult<T>> Fetcher<T extends Jsonable>(int page);

// TODO: is this a good home. Is hateoas_models better? somewhere else?

/// Represents a Paged [SearchResult]. The [Page]s can be traversed starting
/// with [firstPage] or [all] pages can be fetched
class Paged<T> {
  final Fetcher<T> _fetch;
  final int _estimatedTotalSize;

  Paged(this._fetch, [this._estimatedTotalSize = 10]);

  Future<Page<T>> get firstPage => Page.create(_fetch);

  FetchAll<T> get all {
    final sc = new StreamController<SearchResult>.broadcast();

    var percentComplete =
        new PercentComplete().setTotal(_estimatedTotalSize.toDouble());

    final percentCompleteProperty = new ControllableProperty(percentComplete);

    doFetch(Option<Future<Page>> pageOpt) {
      pageOpt.map((pageFuture) => pageFuture.then((Page page) {
        percentComplete =
            percentComplete.setTotal(page.result.totalCount.toDouble());
        percentComplete =
            percentComplete.addToCurrent(page.result.results.length.toDouble());

        percentCompleteProperty.value = percentComplete;

        sc.add(page.result);
        doFetch(page.nextPage);
      }).catchError((e, st) {
        sc.addError(e, st);
        sc.close();
      })).getOrElse(() {
        percentComplete.complete();
        sc.close();
      });
    }

    doFetch(new Some(Page.create(_fetch)));

    return new FetchAll(percentCompleteProperty.distinctProperty, sc.stream);
  }
}

/// TODO: shit name.
/// Represents a stream of [SearchResult]s as a result of fetching all [Page]s
/// in a search. [percentComplete] provides feedback on how far through the
/// pages the search is
class FetchAll<T> {
  final Property<PercentComplete> percentComplete;
  final Stream<SearchResult<T>> resultStream;

  FetchAll(this.percentComplete, this.resultStream);
}

/// Represents a single page of [SearchResult]s.
/// The other pages can be traversed following the [nextPage] and [previousPage]
/// properties
class Page<T> {
  final Fetcher<T> _fetcher;
  final SearchResult<T> result;

  Page._(this._fetcher, this.result);

  static Future<Page> create(Fetcher fetch) => _create(fetch, 1);

  static Future<Page> _create(Fetcher fetch, int page) {
    return fetch(page).then((sr) {
      return new Page._(fetch, sr);
    });
  }

  Option<Future<Page<T>>> get nextPage => result.numPages > result.page
      ? new Some(_create(_fetcher, result.page + 1))
      : const None();

  Option<Future<Page<T>>> get previousPage => result.page > 1
      ? new Some(_create(_fetcher, result.page - 1))
      : const None();
}
