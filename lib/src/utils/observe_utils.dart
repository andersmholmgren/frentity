library observe.utils;

import 'package:observe/observe.dart';
import 'package:frappe/frappe.dart';
import 'dart:async';
import 'frappe_utils.dart';
import 'package:option/option.dart';
import 'package:frentity/src/utils/preconditions.dart';

ObservableList derivedObservableList(ObservableList observedList, f(element)) =>
    _derivedObservableList(observedList, (i) => i.map(f));

ObservableList derivedObservableListWithExpand(
        ObservableList observedList, Iterable f(element)) =>
    _derivedObservableList(observedList, (i) => i.expand(f));

typedef Iterable _IterOp(Iterable i);

ObservableList _derivedObservableList(ObservableList observedList, _IterOp op) {
  final derivedList = new ObservableList.from(op(observedList));

  observedList.listChanges.listen((List<ListChangeRecord> crs) => crs
      .forEach((cr) {
    if (cr.removed.isNotEmpty) {
      final targetRemoveLength = op(cr.removed).length;
      derivedList.removeRange(cr.index, cr.index + targetRemoveLength);
    }

    if (cr.addedCount > 0) {
      final addedValues =
          observedList.sublist(cr.index, cr.index + cr.addedCount);
      derivedList.insertAll(cr.index, op(addedValues));
    }
  }));

  return derivedList;
}

ObservableList aggregatedObservableList(Iterable<ObservableList> lists) {
  final ObservableList observableLists =
      lists is ObservableList ? lists : new ObservableList.from(lists);

  final derivedList = new ObservableList();
  final Map<ObservableList, StreamSubscription> subscriptions = {};

  _itemsAddedToAggregatedObservableList(
      observableLists, observableLists, derivedList, subscriptions);

  observableLists.listChanges.listen((List<ListChangeRecord> crs) => crs
      .forEach((cr) {
    if (cr.removed.isNotEmpty) {
      cr.removed.forEach((ObservableList r) {
        final subscription = subscriptions.remove(r);
        subscription.cancel();
      });

      final int targetRemoveIndex =
          lists.take(cr.index).fold(0, (total, l) => total + l.length);
      final int targetRemoveLength =
          cr.removed.fold(0, (total, l) => total + l.length);

      // TODO: start index needs to be offset too

//          print('********** removing: start: ${targetRemoveIndex}, end: ${targetRemoveIndex + targetRemoveLength}');
//          print(cr.removed);
      derivedList.removeRange(
          targetRemoveIndex, targetRemoveIndex + targetRemoveLength);
    }

    if (cr.addedCount > 0) {
      final addedLists = cr.object.sublist(cr.index, cr.index + cr.addedCount);

      _itemsAddedToAggregatedObservableList(
          addedLists, observableLists, derivedList, subscriptions);
    }
  }));

  return derivedList;
}

void _itemsAddedToAggregatedObservableList(Iterable<ObservableList> addedLists,
    ObservableList observableLists, ObservableList aggregatedList,
    Map<ObservableList, StreamSubscription> subscriptions) {
  addedLists.forEach((list) {
    aggregatedList.addAll(list);
  });

  int _listOffset(list) {
    return observableLists.takeWhile((l) => l != list).fold(
        0, (t, l) => t + l.length);
  }

  addedLists.forEach((observedList) {
    final subscription = observedList.listChanges.listen(
        (List<ListChangeRecord> crs) => crs.forEach((cr) {
      if (cr.removed.isNotEmpty) {
        final aggregatedIndex = _listOffset(observedList) + cr.index;
        aggregatedList.removeRange(
            aggregatedIndex, aggregatedIndex + cr.removed.length);
      }

      if (cr.addedCount > 0) {
        final addedValues =
            cr.object.sublist(cr.index, cr.index + cr.addedCount);
        final aggregatedIndex = _listOffset(observedList) + cr.index;
        aggregatedList.insertAll(aggregatedIndex, addedValues);
      }
    }));

    subscriptions[observedList] = subscription;
  });
}

typedef Keyable(value);

ObservableMap derivedObservableMap(ObservableList list, Keyable keyable) {
  final derivedMap = new ObservableMap();
  list.forEach((value) {
    derivedMap[keyable(value)] = value;
  });

  list.listChanges.listen((List<ListChangeRecord> crs) => crs.forEach((cr) {
    cr.removed.forEach((value) {
      derivedMap.remove(keyable(value));
//      print('removed value $value: $removed; ${keyable(value)}');
    });

    if (cr.addedCount > 0) {
      final addedValues = list.sublist(cr.index, cr.index + cr.addedCount);
      addedValues.forEach((value) {
        derivedMap[keyable(value)] = value;
      });
    }
  }));

  return derivedMap;
}

EventStream mergeEventStreamsFromObservable(
    ObservableList<EventStream> eventStreams) {
  final Property<EventStream> p =
      _createMergedEventStreamProperty(eventStreams);

  final ces = new ControllableEventStream.std();

  StreamSubscription currentEventStreamSubscription;

  p.listen((EventStream newStream) {
    final newSubscription = newStream.listen((event) {
      ces.controller.add(event);
    });
    if (currentEventStreamSubscription != null) {
      currentEventStreamSubscription.cancel();
    }
    currentEventStreamSubscription = newSubscription;
  });

  return ces.eventStream;
}

Property<EventStream> _createMergedEventStreamProperty(
    ObservableList<EventStream> eventStreams) {
  // TODO: this can be rewritten to use derivePropertyFromObservableList
  EventStream merge() => eventStreams.fold(
      new EventStream(new Stream.fromIterable([])), (c, n) => c.merge(n));

  final ces = new ControllableEventStream.std();
  eventStreams.listChanges.listen((_) {
    ces.controller.add(merge());
  });
  return new Property.fromStreamWithInitialValue(merge(), ces.eventStream);
}

// TODO: is it better to do extends Property<T> with Observable??
class ObservableFRProperty<T> extends Observable {
  final Property<T> property;

  @observable T value;

  ObservableFRProperty(this.property) {
    ensure(property, isNotNull);
    property.listen((T v) {
      value = v;
    });
  }
}

class ObservableOptionalFRProperty<T> extends Observable {
  final Property<Option<T>> property;

  @observable T value;

  ObservableOptionalFRProperty(this.property) {
    ensure(property, isNotNull);
    property.listen((Option<T> o) {
//      print('---------- $o');
      if (o is Some) {
        value = o.get();
      } else {
        value = null;
      }
    });
  }
}

ObservableFRProperty observableToFRProperty(
    Observable source, Symbol sourceName, dynamic read()) {
  return new ObservableFRProperty(
      observableToProperty(source, sourceName, read));
}

Property observableToProperty(
    Observable source, Symbol sourceName, dynamic read()) {
  // TODO: is this a leak??
  final sc = new StreamController();
  onPropertyChange(source, sourceName, () {
    sc.add(read());
  });
  return new Property.fromStreamWithInitialValue(read(), sc.stream);
}

ObservableList propertyListToObservableList(Property<List> p) {
  ObservableList ol = new ObservableList();

  p.listen((List l) {
    final changes = ObservableList.calculateChangeRecords(ol, l);
    print('-------- changes: # ${changes.length}: $changes');
    ObservableList.applyChangeRecords(ol, l, changes);
  });

  return ol;
}

typedef List ListTransform(List initial);

Property<List> observableListToPropertyList(ObservableList ol) {
  List latest() => []..addAll(ol);

  return new Property.fromStreamWithInitialValue(
      latest(), ol.listChanges.map((crs) => latest()));
}
