library frappe.utils;

import 'package:frappe/frappe.dart';
import 'dart:async';
import 'package:quiver/collection.dart';

typedef Reactable MapToReactable(v);

Property<Iterable> mapPropertyIterable(
        Property<Iterable> propertyContainers, MapToReactable mapper) =>
    mapReactableIterable(propertyContainers, mapper).asProperty();

Reactable<Iterable> mapReactableIterable(
    Reactable<Iterable> reactableContainers, MapToReactable mapper) {
  final Reactable<Iterable<Reactable>> pip =
      reactableContainers.map((Iterable i) => i.map(mapper));

//  pip.listen((v) => print('pip (${identityHashCode(pip)}) $v'));

  final Reactable<Iterable> pi =
      _reactableIterableReactableToReactableIterable(pip);

  return pi;
}

Property<Iterable> iterablePropertiesToPropertyIterable(
        Iterable<Property> properties) =>
    iterableReactablesToReactableIterable(properties).asProperty();

Reactable<Iterable> iterableReactablesToReactableIterable(
    Iterable<Reactable> properties) {
  final empty = new Property.constant([]);

  final Reactable<Iterable> combined = properties.fold(empty, (r, p) => r
      .combine(p, (l, v) => []
    ..addAll(l)
    ..add(v)));

//  combined.listen((v) => print('combined  $v'));

  // TODO: I suspect this asBroadcastStream is needed due to timing.
  // This may only be in tests. Not sure if this is needed in real life
  return combined.distinct(listsEqual).asBroadcastStream();
}

Reactable<Iterable> _reactableIterableReactableToReactableIterable(
    Reactable<Iterable<Reactable>> pip) {
  final Reactable<Reactable<Iterable>> ppi =
      pip.map(iterableReactablesToReactableIterable);

//  ppi.listen((v) => print('ppi $v'));

  final Reactable<Iterable> result = ppi
      .flatMapLatest((Reactable<Iterable> pi) {
//    print('^^^^^^^^ $pi (${identityHashCode(pi)})');
    final x = pi;
//      x.listen((v) => print('x $v'));
    return x;
  });

  final f = result;

//  f.listen((v) => print('f $v'));

  return f;
}

class ControllableEventStream<T> {
  final StreamController<T> controller;
  EventStream<T> _eventStream;
  EventStream<T> get eventStream => _eventStream;

  void add(T value) => controller.add(value);

  ControllableEventStream._internal(this.controller) {
    _eventStream = new EventStream(controller.stream);
  }

  ControllableEventStream.std()
      : this._internal(new StreamController.broadcast());
}

class ControllableProperty<T> {
  final ControllableEventStream<T> controllable;
  final Property<T> property;
  Property<T> get distinctProperty => property.distinct();

  void set value(T v) {
//    print('==========$v');
    controllable.controller.add(v);
  }

  ControllableProperty._(
      T initialValue, ControllableEventStream<T> controllable)
      : this.controllable = controllable,
        this.property = initialValue != null
            ? new Property.fromStreamWithInitialValue(
                initialValue, controllable.eventStream)
            : new Property.fromStream(controllable.eventStream);

  ControllableProperty([T initialValue])
      : this._(initialValue, new ControllableEventStream.std());
}
