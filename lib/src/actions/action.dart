library actions.core;

import 'dart:async';
import 'package:frentity/src/utils/preconditions.dart';
import 'package:frappe/frappe.dart';

/// thrown to abort an action prior to redirecting the browser. The action
/// won't complete
class AbortAction {}

typedef Future<T> Callable<T>();

abstract class Action<T> {
  String get completionMessage;
//  bool get parallelisable => false;
//  bool get cancelable => false;

  // TODO: bad name. Confusing with merge
  Action combine(Action other);

  Callable<T> get task;

  Future<T> get completionFuture;

//  // TODO: this is all so I can avoid exposing the completion methods on Action
//  factory Action(Callable<T> task, String completionMessage) =>
//    new SingleAction(task, completionMessage);

  void complete(T result);

  void completeError(e, stackTrace);
}

abstract class LoadAction<T> implements Action<T> {}

abstract class UndoableAction<T> implements Action<T> {
  Action get undoAction;
}

//abstract class MultistepAction<T> implements Action<T> {
//  double get percentComplete;
//}

abstract class MergableAction implements Action {
  MergableAction merge(MergableAction other);
}

//abstract class ParallelisableAction implements Action {
//}

class CompletedAction<T> {
  final Action<T> action;
  final ActionResult<T> result;

  CompletedAction(this.action, this.result);

  String toString() => 'completed action $action with result $result';
}

abstract class CompletedActionSource {
  EventStream<CompletedAction> get completedActions;
}

abstract class _ActionResultMixin {
  DateTime get actionStart;
  DateTime get actionEnd;
  Duration get actionDuration => actionEnd.difference(actionStart);
}

abstract class ActionResult<T> extends Object with _ActionResultMixin {
  final DateTime actionStart;
  final DateTime actionEnd;

  ActionResult(this.actionStart, this.actionEnd);
}

class ActionSuccess<T> extends ActionResult<T> {
  final T result;

  ActionSuccess(DateTime actionStart, DateTime actionEnd, this.result)
      : super(actionStart, actionEnd);

  String toString() => 'success';
}

class ActionFailure extends ActionResult {
  final String message;

  ActionFailure(DateTime actionStart, DateTime actionEnd, this.message)
      : super(actionStart, actionEnd);

  String toString() => 'failure ($message)';
}

class RepeatedActionFailure extends Object
    with _ActionResultMixin
    implements ActionFailure {
  final ActionFailure initialFailure;
  final ActionFailure latestActionFailure;
  final int failCount;

  RepeatedActionFailure(
      this.initialFailure, this.latestActionFailure, this.failCount);

  @override
  String get message => 'blah blah'; // TODO: WTF

  @override
  DateTime get actionStart => initialFailure.actionStart;

  @override
  DateTime get actionEnd => latestActionFailure.actionEnd;
}

class PercentComplete {
  final double _current;
  final double _total;
  final bool isComplete;

  double get percentComplete =>
      _total == 0.0 ? isComplete ? 1.0 : 0.0 : _current / _total;

  String get _percentCompleteAsStr =>
      (percentComplete * 100).toStringAsFixed(2);

  PercentComplete._(this._current, this._total, this.isComplete) {
    ensure(_current, isNotNull);
    ensure(_total, isNotNull);
    ensure(_total, greaterThanOrEqualTo(0.0));
  }

  PercentComplete() : this._(0.0, 0.0, false);

  PercentComplete setTotal(double newTotal) =>
      new PercentComplete._(_current, newTotal, _current == newTotal);

  PercentComplete addToCurrent(double newAmount) {
    final amount = _current + newAmount;
    return new PercentComplete._(amount, _total, amount == _total);
  }

  PercentComplete combine(PercentComplete other) => new PercentComplete._(
      _current + other._current,
      _total + other._total,
      isComplete && other.isComplete);

  PercentComplete complete() => new PercentComplete._(_total, _total, true);

  String toString() => '$_percentCompleteAsStr% ($_current of $_total)';

  bool operator ==(other) => other is PercentComplete &&
      other._current == _current &&
      other._total == _total;
}
