library actions.executors;

import 'action.dart';
import 'dart:async';

abstract class ActionExecutor {
  Future<CompletedAction> execute(Action action);
}
