library actions.core.impl;

import 'dart:async';
import 'action.dart';

abstract class BaseAction<T> implements Action<T> {
  final Completer<T> _completer = new Completer<T>();

  Action combine(Action other) => new CompositeAction([]
    ..addAll(_actions)
    ..add(other));

  List<Action> get _actions => [this];

  Future<T> get completionFuture => _completer.future;

  void complete(T result) {
    _completer.complete(result);
  }

  void completeError(e, stackTrace) {
    _completer.completeError(e, stackTrace);
  }

  String toString() => "'$completionMessage'";
}

class SimpleAction<T> extends BaseAction<T> {
  @override
  final String completionMessage;

  @override
  final Callable<T> task;

  SimpleAction(this.task, this.completionMessage);
}

class SimpleLoadAction<T> extends SimpleAction<T> implements LoadAction<T> {
  SimpleLoadAction.def(Future<T> task(), String entityDescription)
      : super(task, '$entityDescription loaded');

  SimpleLoadAction(Future<T> task(), String completionMessage)
      : super(task, completionMessage);
}

abstract class DefaultAction<T> extends BaseAction<T> {
  @override
  Callable<T> get task => execute;

  Future<T> execute();
}

abstract class BaseUndoableAction implements UndoableAction {
  Action get mainAction;

  @override
  Action combine(Action other) => mainAction.combine(other);

  @override
  Future get completionFuture => mainAction.completionFuture;

  @override
  String get completionMessage => mainAction.completionMessage;

  @override
  Callable get task => mainAction.task;

  @override
  void complete(result) {
    mainAction.complete(result);
  }

  @override
  void completeError(e, stackTrace) {
    mainAction.completeError(e, stackTrace);
  }
}

class DefaultUndoableAction extends BaseUndoableAction {
  final Action mainAction;

  @override
  final Action undoAction;

  DefaultUndoableAction(this.mainAction, this.undoAction);
}

//abstract class MultistepActionMixin<T> implements MultistepAction<T> {
////  double get percentComplete;
//
////  Iterable<Callable<T>> get steps;
////
//
//  // TODO: This won't have retries for each step!!!!!
//  @override
//  Callable<T> get task {
//    return () {
//      Option<Future<T>> _runStep(Option<Callable<T>> currentStep) {
//        return currentStep.map((Callable<T> step) {
//          return step().then((T result) {
//            return _runStep(nextStep(result));
//          });
//        });
//      }
//
//      return _runStep(new Some(firstStep())).get();
//    };
//  }
//
//  Callable<T> get firstStep;
//
//  Option<Callable<T>> nextStep(T previousResult);
//
//}

class CompositeAction extends DefaultAction {
  final List<Action> _children;

  @override
  String get completionMessage => 'Multiple actions completed';

  @override
  List<Action> get _actions => _children;

  CompositeAction(this._children);

  @override
  Future execute() {
    return null;
//    _mergedChildren
    // execute one at a time as sync manager does
  }

//  Iterable<Action> get _mergedChildren => _children.fold([],
//      (List<Action> result, action) {
//    if (result.isEmpty ||
//        result.last is! MergableAction ||
//        action is! MergableAction) {
//      return result..add(action);
//    } else {
//      final MergableAction last = result.last;
//      return []
//        ..addAll(result.take(result.length - 1))
//        ..add(last.merge(action));
//    }
//  });
//
  // TODO: implement task
  @override
  Callable get task => null;
}
