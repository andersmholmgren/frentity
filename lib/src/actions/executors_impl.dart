library actions.executors.impl;

import 'action.dart';
import 'dart:async';
import 'executors.dart';
import 'package:option/option.dart';
import 'dart:collection';

class SerialActionExecutor implements ActionExecutor {
  final ActionQueue _actions;

  SerialActionExecutor(this._actions);

  SerialActionExecutor.simple() : this(new SimpleActionQueue());

  Future<CompletedAction> execute(Action action) {
    new Future(_process); // trigger processing async
    return _actions.add(action);
  }

  void _process() {
    _actions.next.map((action) {
      final startTime = new DateTime.now();
      action.task().then((result) {
        _actions.actionSucceeded(startTime, new DateTime.now(), result);
      }).catchError((error, stackTrace) {
        _actions.actionAborted(startTime, new DateTime.now());
      }, test: (e) => e is AbortAction).catchError((error, stackTrace) {
        _actions.actionFailed(startTime, new DateTime.now(), error, stackTrace);
      }).whenComplete(() {
        _process();
      });
    });
  }
}

abstract class ActionQueue {
  Option<Action> get next;

  Future<CompletedAction> add(Action action);

  void actionFailed(DateTime startTime, DateTime endTime, error, stackTrace);

  void actionSucceeded(DateTime startTime, DateTime endTime, result);

  void actionAborted(DateTime startTime, DateTime dateTime);
}

class SimpleActionQueue implements ActionQueue {
  final Queue<ActionWrapper> _actions = new Queue();
  Option<ActionWrapper> _pending;

  @override
  void actionFailed(DateTime startTime, DateTime endTime, error, stackTrace) {
    // retries here
    _pending.map((a) => a.failed(startTime, endTime, error, stackTrace));
    _pending = const None();
  }

  @override
  void actionSucceeded(DateTime startTime, DateTime endTime, result) {
    _pending.map((a) => a.succeeded(startTime, endTime, result));
    _pending = const None();
  }

  @override
  void actionAborted(DateTime startTime, DateTime endTime) {
    _pending.map((a) => a.aborted(startTime, endTime));
    _pending = const None();
  }

  @override
  Future<CompletedAction> add(Action action) {
    final wrapped = new ActionWrapper(action);
    _actions.add(wrapped);
    return wrapped.completionFuture;
  }

  @override
  Option<Action> get next {
    _pending =
        _actions.isNotEmpty ? new Some(_actions.removeFirst()) : const None();
    return _pending.map((w) => w.action);
  }
}

class ActionWrapper {
  final Action action;
  final Completer<CompletedAction> _completer = new Completer();

  ActionWrapper(this.action);

  Future<CompletedAction> get completionFuture => _completer.future;

  void succeeded(DateTime startTime, DateTime endTime, result) {
    /*
     * TODO: two futures here, one on the action itself intended for code
     * that creates the actions, the other on the executor which is intended
     * for publishing as a stream of results to observers. Make sense?
     */
    _completer.complete(new CompletedAction(
        action, new ActionSuccess(startTime, endTime, result)));

    action.complete(result);
  }

  void failed(DateTime startTime, DateTime endTime, error, stackTrace) {
    // TODO: completing successfully - make sense?
    _completer.complete(new CompletedAction(action,
        new ActionFailure(startTime, endTime, _message(error, stackTrace))));

    action.completeError(error, stackTrace);
  }

  void aborted(DateTime startTime, DateTime endTime) {
    // Do nothing. Abort should only be used prior to a redirect
  }

  String _message(dynamic error, dynamic stackTrace) {
    return error.toString(); // TODO
  }
}
