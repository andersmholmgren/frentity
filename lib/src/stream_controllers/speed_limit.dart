library frentity.stream_transformers;

import 'dart:async';
import 'dart:math';

/// Throttles the delivery of each event such that they are separated by at
/// least the given duration. Errors occurring
/// on the source stream will not be delayed. If the source stream is a broadcast
/// stream, then the transformed stream will also be a broadcast stream.
///
/// **Example:**
///
///     var controller = new StreamController();
///     var slowed = controller.stream.transform(new SpeedBump(new Duration(seconds: 2)));
///
///     // source:             as-d---f----g-
///     // source.slowed(2):   a--s--d--f--g-
class SpeedLimit<T> implements StreamTransformer<T, T> {
  final Duration _minDelay;
  DateTime _lastEventTime;

  SpeedLimit(Duration minDelay) : _minDelay = minDelay;

  Stream<T> bind(Stream<T> stream) {
    return stream.asyncMap((event) {
      final now = new DateTime.now();
      final elapsed =
          _lastEventTime != null ? now.difference(_lastEventTime) : _minDelay;
      final delay = new Duration(
          microseconds: max(
              _minDelay.inMicroseconds - elapsed.inMicroseconds, 0));
      _lastEventTime = now;
      return new Future.delayed(delay, () => event);
    });
  }
}
